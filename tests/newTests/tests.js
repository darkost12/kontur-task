const chai = require('chai');
const path = require('path');
const child = require('child_process');

const { expect } = chai;
let proc;
const exec = path.join(__dirname, '../..', 'index.js');

describe('Команда show', () => {
    before(() => {
        process.chdir(__dirname);
    });

    beforeEach(() => {
        proc = child.exec('node ' + exec);
    });

    it('должен показывать список todo v2', (done) => {
        const result = `
  !  |  user        |  date        |  comment                                             |  fileName       
------------------------------------------------------------------------------------------------------------
  !  |              |              |  Hi!                                                 |  jsWithTodo.js  
     |              |              |  Как дела?                                           |  jsWithTodo.js  
  !  |  Veronika    |  2018-12-25  |  С Наступающим 2019!                                 |  jsWithTodo.js  
  !  |  Pe          |  2018-12-26  |  Работать пора!                                      |  jsWithTodo.js  
  !  |  Pe          |  2018-12-26  |  Работать пора!                                      |  jsWithTodo.js  
  !  |  Pe          |  2018-12-26  |  Работать пора!                                      |  jsWithTodo.js  
     |              |  2018-10-01  |  Можно ли написать более лаконично?                  |  jsWithTodo.js  
     |  pe          |  2015-08-10  |  а какая будет кодировка?                            |  jsWithTodo.js  
  !  |  digi        |  2016-04-08  |  добавить writeLine!!!                               |  jsWithTodo.js  
     |  PE          |  2018-08-20  |  переименовать?                                      |  jsWithTodo.js  
     |  Anonymo...  |  2016-03-17  |  Необходимо переписать этот код и использовать а...  |  jsWithTodo.js  
     |  WinDev      |              |  Убедиться, что будет работать под Windows.          |  jsWithTodo.js  
     |  Veronika    |  2018-08-16  |  сделать кодировку настраиваемой                     |  jsWithTodo.js  
     |  Digi        |  2018-09-21  |  Добавить функцию getFileName, которая по пути ф...  |  jsWithTodo.js  
------------------------------------------------------------------------------------------------------------
`.trim();
        proc.stdout.once('data', function(){
            proc.stdin.write('show\r');
            proc.stdout.once('data', function(output){
                expect(output.toString('utf-8').trim()).to.eq(result);
                done();
            });
        });
    });
});
